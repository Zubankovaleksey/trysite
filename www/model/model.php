<?php
class Page
{
    public static  function getCookieName()
    {
        return $_COOKIE["name"];
    }
    public static  function getCookieID()
    {
        return $_COOKIE["id"];
    }
    public static function isAdmin($id)
    {
        $db = Db::getConnection();
        $query = mysqli_query($db, "SELECT * FROM `users` WHERE id='{$id}'");
        $row=mysqli_fetch_assoc($query);
        $answ=false;
        if($row['IsAdmin']=="1") {
            $answ = true;
        }
        return $answ;
    }
    public static function InsertUser($user)
    {
        $db = Db::getConnection();
        $query = mysqli_query($db, "SELECT * FROM `users` WHERE login='{$user['login']}'");
        $numr = mysqli_num_rows($query);
        $res=false;
        if ($numr == 0) {
            $sql_q = "INSERT INTO `users`
 (name,login,password, IsAdmin)
 VALUES('{$user['name']}','{$user['login']}', '{$user['pass']}', 0)";
            $res = mysqli_query($db, $sql_q);
        }
        return $res;
    }
    public static function InsertNew($new)
    {
        $db = Db::getConnection();
        $sql_q = "INSERT INTO `news`
 (name,text,author,date, Anons)
 VALUES('{$new['name']}','{$new['text']}', '{$new['author']}', '{$new['date']}', '{$new['anons']}')";
        $res = mysqli_query($db, $sql_q);
        if($res)
        {

            Page::UpdateNew($new);
        }
        return $res;
    }
    public static function deleteNew($id)
    {
        $db = Db::getConnection();
        $sql = "DELETE FROM news WHERE id=$id";
        $res = mysqli_query($db, $sql);
        return $res;
    }
    public static function UpdateNew($new)
    {
        $news = self::GetInfo();
        $count = count($news);
        if($new["id"]!==null) {
            for ($i = 0; $i <= count($news); $i++)
            {
                if ($news[$i]["id"] === $new["id"]) {
                    $new["PictureFull"]=$news[$i]["PictureFull"];
                    $new["PictureAnons"]=$news[$i]["PictureAnons"];
                }
            }
        }
        else {
            $news = $news[$count - 1];
            $new=$news;
        }
        if($_FILES['f']['tmp_name']==="" && ($new["PictureFull"] === "" || $new["PictureFull"]==="photo.jpg")) {
            $_FILES['f']['tmp_name'] = "Z:\\home\\site.ru\\www\\img\\photo.jpg";
            $dest = "Z:\\home\\site.ru\\www\\img\\".$new['id'].".jpg";
            @copy($_FILES["f"]["tmp_name"], $dest);
            $new['PictureFull']=$new['id'].".jpg";
        }
        elseif ($_FILES['f']['tmp_name']!=="") {
            $type = $_FILES['f']['name'];
            $type = explode(".", $type);
            $count = count($type);
            $dest = "Z:\\home\\site.ru\\www\\img\\" . $new['id'] . "." . $type[$count - 1];
            @copy($_FILES["f"]["tmp_name"], $dest);
            $new['PictureFull']=$new['id'].".".$type[$count-1];
        }

        if($_FILES['a']['tmp_name']===""&&($new["PictureAnons"] === "" || $new["PictureAnons"]==="photo.jpg")) {
            $_FILES['a']['tmp_name'] = "Z:\\home\\site.ru\\www\\img\\anons\\photo.jpg";
            $dest = "Z:\\home\\site.ru\\www\\img\\anons\\".$new['id'].".jpg";
            @copy($_FILES["a"]["tmp_name"], $dest);
            $new['PictureAnons']=$new['id'].".jpg";
        }
        elseif($_FILES['a']['tmp_name']!=="")
        {
            $type = $_FILES['a']['name'];
            $type = explode(".",$type);
            $count = count($type);
            $dest = "Z:\\home\\site.ru\\www\\img\\anons\\".$new['id'].".".$type[$count-1];
            @copy($_FILES["a"]["tmp_name"], $dest);
            $new['PictureAnons']=$new['id'].".".$type[$count-1];
        }

        $db = Db::getConnection();
        $sql = "Update news set name='{$new['name']}',text='{$new['text']}',author='{$new['author']}',date='{$new['date']}',PictureFull='{$new['PictureFull']}', Anons='{$new['anons']}',PictureAnons='{$new['PictureAnons']}' WHERE id='{$new['id']}';";
        $res = mysqli_query($db, $sql);
        return $res;
    }
    public static function GetInfo()
    {
        $link = Db::getConnection();

        $query = mysqli_query($link,"SELECT * FROM news");
        $index = array();
        $i=0;
        while($row=mysqli_fetch_assoc($query)) {
            $index[$i]['id'] = $row['id'];
            $index[$i]['name'] = $row['name'];
            $index[$i]['text'] = $row['text'];
            $index[$i]['date'] = $row['date'];
            $index[$i]['author'] = $row['author'];
            $index[$i]['PictureFull'] = $row['PictureFull'];
            $index[$i]['Anons'] = $row['Anons'];
            $index[$i]['PictureAnons'] = $row['PictureAnons'];
            $i++;
        }
        return $index;
    }
    public static function GetNews($NumberOfTen)
    {
        $link = Db::getConnection();
        $begin = $NumberOfTen*10;
        $query = mysqli_query($link,"SELECT * FROM news LIMIT $begin, 10");
        $index = array();
        $i=0;
        while($row=mysqli_fetch_assoc($query)) {
            $index[$i]['id'] = $row['id'];
            $index[$i]['name'] = $row['name'];
            $index[$i]['text'] = $row['text'];
            $index[$i]['date'] = $row['date'];
            $index[$i]['author'] = $row['author'];
            $index[$i]['PictureFull'] = $row['PictureFull'];
            $index[$i]['Anons'] = $row['Anons'];
            $index[$i]['PictureAnons'] = $row['PictureAnons'];

            $i++;
        }
        return $index;

    }
    public static function GetNew($id)
    {
        $link = Db::getConnection();

        $query = mysqli_query($link,"SELECT * FROM news where id = '{$id}'");
        $index = array();
        $i=0;
        $row=mysqli_fetch_assoc($query);
        $index['id']=$row['id'];
        $index['name'] = $row['name'];
        $index['text'] = $row['text'];
        $index['date'] = $row['date'];
        $index['author'] = $row['author'];
        $index['PictureFull'] = $row['PictureFull'];
        $index['Anons'] = $row['Anons'];
        $index['PictureAnons'] = $row['PictureAnons'];

        return $index;
    }

    public static function UpdateInfo($id, $name)
    {
        $db = Db::getConnection();
        $sql = 'UPDATE `table` SET name=:name_bind WHERE id=:id_bind';
        $result = $db->prepare($sql);
        $result->bindParam(':id_bind',$id, PDO::PARAM_INT);
        $result->bindParam(':name_bind',$name, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function DeleteInfo($id)
    {
        $db = Db::getConnection();
        $sql = 'DELETE FROM `table` WHERE id=:id_bind';
        $result = $db->prepare($sql);
        $result->bindParam(':id_bind',$id, PDO::PARAM_INT);
        return $result->execute();
    }
    public static function GetCountNews()
    {
        $link = Db::getConnection();

        $query = mysqli_query($link,"SELECT COUNT(*) FROM news");
        $count = mysqli_fetch_array($query);
        return $count[0];
    }
}
Class  DB
{
    public static function getConnection()
    {
        $link=mysqli_connect("localhost", "root", "", "site");
        return $link;
    }
}
?>