<?php

if (!class_exists('Page')) {
    require_once "model/model.php";
}
if (!class_exists('Core')) {
    require_once "core.php";
}


class Controller {
    /** @var Core $core */
    public $core;


    /**
     * Конструктор класса, требует передачи Core
     *
     * @param Core $core
     */
    function __construct(Core $core) {
        $this->core = $core;
    }
    public function initialize(array $params = array()) {

        return true;
    }

    public function redirect($url = '/') {
        header("Location: {$url}");
        exit();
    }

}