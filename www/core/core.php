<?php

class Core {
    public $config = array();


    /**
     * Конструктор класса
     *
     * @param array $config
     */
    function __construct(array $config = array()) {
        $this->config = array_merge(
            array(
                'controllersPath' => dirname(__FILE__) . '/controllers/',
            ),
            $config
        );
    }
    /**
     * Обработка входящего запроса
     *
     * @param $uri
     */
    public function handleRequest($uri) {
        // Определяем страницу для вывода
        $request = explode('/', $uri);
        // Имена контроллеров у нас с большой буквы
        $name = ucfirst($request[0]);
        // Полный путь до запрошенного контроллера
        $file = $this->config['controllersPath'] . $name . '.php';
        if(isset($_POST['LoginButton']))
        {
            $file = $this->config['controllersPath'] .'Login.php';
            $name = "Login";
        }
        if(isset($_POST['singUpButton']))
        {
            $file = $this->config['controllersPath'] .'SignUp.php';
            $name = "SignUp";
        }

        // Если нужного контроллера нет, то используем контроллер Home
        if (!file_exists($file)) {
            $file = $this->config['controllersPath'] . 'Home.php';
            // Определяем имя класса, согласно принятым у нас правилам
            $class = 'Controllers_Home';
        }
        else {
            $class = 'Controllers_' . $name;
        }

        // Если контроллер еще не был загружен - загружаем его
        if (!class_exists($class)) {
            require_once $file;
        }
        // И запускаем
        $controller = new $class($this);
        $request['core'] = "../core.php";
        $initialize = $controller->initialize($request);
        if ($initialize === true) {
            $response = $controller->run();
        }
        elseif (is_string($initialize)) {
            $response = $initialize;
        }
        else {
            $response = 'Возникла неведомая ошибка при загрузке страницы';
        }

        return $response;
    }
    public function view($filename="")
    {
        $name = explode('.', $filename);

        if(!class_exists($name[0]))
            include "view/".$filename;
        return new $name[0]();
    }
}