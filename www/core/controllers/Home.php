<?php
if(!class_exists('Controller'))
{
    require_once "core/Controller.php";
}
class Controllers_Home extends Controller {
    public function initialize(array $params = array()) {
        if (!empty($_REQUEST['q'])) {
            $this->redirect('/');
        }
        return true;
    }
    /**
     * Основной рабочий метод
     *
     * @return string
     */
    public function run() {
        $index = Page::GetInfo();
        $view = Core::view("ViewHome.php");
        $array='array';
        $name = 'name';
        $id = 'id';
        $admin = 'isAdmin';
        $view->$array=$index;
        $view->$name =$_COOKIE['name'];
        $view->$id =$_COOKIE['id'];
        $view->$admin =Page::isAdmin($_COOKIE['id']);
        $view->render();
    }

}