<?php

if(!class_exists('Controller'))
{
    require_once "core/Controller.php";
}

class Controllers_SignUp extends Controller {


    public function initialize(array $params = array()) {
        if (empty($params)) {
            $this->redirect('/SignUp/');
        }
        return true;
    }
    /**
     * Основной рабочий метод
     *
     * @return string
     */
    public function run() {
        $name='';

        $user['name'] = $_POST['nameSingUp'];
        $user['login'] = $_POST['loginSingUp'];
        $user['pass'] = $_POST['passwordSingUp'];
        if(strlen($user['name'])>=4 and strlen($user['name'])<30
            and strlen($user['login'])>=4 and strlen($user['login'])<30
            and strlen($user['pass'])>=4 and strlen($user['pass'])<30)
        {
            $res = Page::InsertUser($user);
            if ($res === true) {
                setcookie("name", $user['name'], time() + 60 * 60 * 24 * 30);
                $_SESSION['name'] = $_POST['loginSingUp'];
                $name = $_SESSION['name'];
                return true;
            } else {
                return false;
            }
        }
        else return "incorrect";
    }

}