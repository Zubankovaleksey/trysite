<?php

if(!class_exists('Controller'))
{
    require_once "core/Controller.php";
}

class Controllers_Logout extends Controller {


    public function initialize(array $params = array()) {
        if (empty($params)) {
            $this->redirect('/login/');
        }
        return true;
    }
    /**
     * Основной рабочий метод
     *
     * @return string
     */
    public function run() {
        unset($_SESSION['name']);
        unset($_SESSION['id']);
        setcookie("name", "", time()-1);
        setcookie("id", "", time()-1);
        $name='';
        header("Location: index.php");
        exit;
    }

}