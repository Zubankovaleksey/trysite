<?php

if(!class_exists('Controller'))
{
    require_once "core/Controller.php";
}
class Controllers_Admin extends Controller
{
    public function initialize(array $params = array()) {
        if (empty($_REQUEST['q'])) {
            $this->redirect('/');
        }
        return true;
    }
    /**
     * Основной рабочий метод
     *
     * @return string
     */
    public function run() {
        if($_REQUEST['number'])
            $news = Page::GetNews($_REQUEST['number']);
        else
            $news = Page::GetNews(0);
        $count = Page::GetCountNews();
        $view = Core::view("ViewAdmin.php");
        $view->count=$count;
        $view->news=$news;
        $view->admin=Page::isAdmin(Page::getCookieID());
        $view->render();
    }
}