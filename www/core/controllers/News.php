<?php

if(!class_exists('Controller'))
{
    require_once "core/Controller.php";
}

class Controllers_News extends Controller {
    public $id;
    public function initialize(array $params = array()) {
        if (empty($_REQUEST['q'])) {
            $this->redirect('/');
        }
        return true;
    }
    /**
     * Основной рабочий метод
     *
     * @return string
     */
    public function run() {
        if(!class_exists('Controllers_Admin'))
        {
            require_once "core/controllers/Admin.php";
        }
        if(!isset($_GET['m']))
        {
            $this->showNew();
        }
        elseif($_GET['m']=='add' or $_GET['m']=='update')
        {
            $this->addNew();
        }
        elseif($_GET['m']=='delete')
        {
            $this->deleteNew();
        }
    }
    private function showNew()
    {
        $news = Page::GetNews(0);
        $index = Page::GetNew($_REQUEST['id']);
        if($index['id']==null)
            $this->redirect('/');
        $view = Core::view("ViewNews.php");
        $view->array=$index;
        $view->news=$news;
        $view->render();
    }
    private function addNew()
    {
        $new=null;
        $admin = Page::isAdmin(Page::getCookieID());
        $view = Core::view("ViewAddNew.php");
        $view->admin=$admin;

        if($_GET['m']=="update")
        {
            $new = Page::GetNew($_GET['id']);
            if($new['id']==null)
                $this->redirect('/');
            $view->new=$new;
            $view->id=$_GET['id'];
            if(isset($_POST['UpdateBtn']))
            {
                $new['name']=$_POST['nameUpd'];
                $new['text'] = $_POST['textUpd'];
                $new['author'] = $_POST['authorUpd'];
                $new['date'] = $_POST['dateUpd'];
                $new['id']=$_POST['idUpd'];
                $new['anons'] = $_POST['Anons'];

                $res=Page::UpdateNew($new);
                Controllers_Admin::run();
                exit();
            }
        }

        if(isset($_POST['UpdateBtn']))
        {
            $new['name']=$_POST['nameUpd'];
            $new['text'] = $_POST['textUpd'];
            $new['author'] = $_POST['authorUpd'];
            $new['date'] = $_POST['dateUpd'];
            $new['anons'] = $_POST['Anons'];
            $res=Page::InsertNew($new);
            Controllers_Admin::run();
            exit();
        }
        $view->render();
    }
    private function deleteNew()
    {

        Page::deleteNew($_GET["id"]);
        Controllers_Admin::run();

    }


}