<?php
if (!class_exists('core')) {
require_once 'core/core.php';
}
$Core = new Core();

$req = !empty($_REQUEST['q'])
? trim($_REQUEST['q'])
: '';
$result = $Core->handleRequest($req);
echo $result;