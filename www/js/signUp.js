jQuery(document).ready(function($) {

    $(".signUpForm").submit(function(e) {
        var str = $(this).serialize();
        str+="&singUpButton=Enter";
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "index.php",
            data: str ,
            success: function(msg) {
                if(msg === "1") {
                    window.location = "index.php";
                } else if(msg === "incorrect")
                {
                    var login = document.getElementById("loginSingUp");
                    var name = document.getElementById("nameSingUp");
                    var pass = document.getElementById("passwordSingUp");
                    var loginInc = document.getElementById("incorrectSignUpLogin");
                    var nameInc = document.getElementById("incorrectSignUpName");
                    var passInc = document.getElementById("incorrectSignUpPassword");

                    if(login.value.toString().length<4 || login.value.toString().length>30)
                    {
                        loginInc.style.display="block";
                    }
                    else
                        loginInc.style.display="none";

                    if(name.value.toString().length<4 || name.value.toString().length>30)
                    {
                        nameInc.style.display="block";
                    }
                    else
                        nameInc.style.display="none";

                    if(pass.value.toString().length<4 || pass.value.toString().length>30)
                    {
                        passInc.style.display="block";
                    }
                    else
                        passInc.style.display="none";

                }
                else{
                    var incorrect = document.getElementById("exist");
                    incorrect.style.display = 'block';
                }
            }
        });
        return false;
    });
});