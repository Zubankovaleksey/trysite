jQuery(document).ready(function($) {

    $(".loginForm").submit(function(e) {
        var str = $(this).serialize();
        str+="&LoginButton=Enter";
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "index.php",
            data: str ,
            success: function(msg) {
                if(msg === "1") {
                    window.location = "index.php";
                }
                else if(msg === "incorrect")
                {
                    var login = document.getElementById("login");
                    var pass = document.getElementById("password");
                    var loginInc = document.getElementById("incorrectLogin");
                    var passInc = document.getElementById("incorrectPassword");

                    if(login.value.toString().length===0)
                    {
                        loginInc.style.display="block";
                    }
                    else
                        loginInc.style.display="none";

                    if(pass.value.toString().length===0)
                    {
                        passInc.style.display="block";
                    }
                    else
                        passInc.style.display="none";

                }else {
                    var incorrect = document.getElementById("incorrectData");
                    incorrect.style.display = 'block';
                }
            }
        });
        return false;
    });
});