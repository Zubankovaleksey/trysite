<?php if($this->admin){?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>The first site</title>
    <base href="http://site.ru/">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>
    <div class="row">
        <div class="col-sm-5">
            <a href="index.php" class="btn btn-primary">return to main</a>
        </div>
        <div class="col-sm-2">
            <a href="/news/add" class="btn btn-primary">Add</a>
        </div>

    </div>
    <table class="table table-striped">
        <tbody>
        <tr>
            <?php
            if($this->count%10===0)
            {
                $count=$this->count/10;
            }
            else
            {
                $count = (int)($this->count/10 +1);
            }
            $currentNumber = $_GET["number"];
            if($currentNumber===null)
                $currentNumber=0;
            foreach ($this->news AS $index):?>
            <th scope="row"></th>
            <td>Name: <a href="/news/<?php echo $index["id"] ?>"><?php echo $index["name"];?></a> <br> Date of create: <?php $date = new DateTime($index["date"]);
            echo $date->format("d.M.Y");?></td>
            <td><a href="/news/delete/<?php echo $index['id']?>">Delete</a> </td>
            <td><a href="/news/update/<?php echo $index['id']?>">Update</a></td>
        </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <div align="center">
    <?php $i=0; while ($i<$count and $count!==1)
        {?>
           <a <?php echo $i===(int)($currentNumber) ? "onclick=\"return false\"" : "" ?> href="/admin/<?php echo $i ?>"><?php echo $i+1 ?> </a>
        <?php $i=$i+1; } ?>
    </div>
</body>
</html>
<?}?>