<?php if($this->admin){?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <base href="http://site.ru/">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<form enctype="multipart/form-data" action="" method="post">
    <div class="container">
        <?php $new=$this->new?>
        <div class="row">
            <div class="col-sm-1">
                <label for="f">Anons</label>
            </div>
            <div class="col-sm-4">
                <p><input type="file" onchange="Anon.call(this)" accept="image/*" id="a" name="a">
            </div>
        </div>
        <div class="row rows" id="AnonsPlace" style="display:<?php echo $new['PictureFull']!=null ?  "block" :  "none";?>">
            <img id="imageAnons" src="<?php echo "img\\".$new['PictureAnons'];?>" class="img-thumbnail image">
            <a id="btnA" onclick="changeAnons()" class="btn btns">X</a>
        </div>
        <style>
            .rows {
                position: relative;
                width: 100%;
                max-width: 400px;
            }

            .rows img {
                width: 100%;
                height: auto;
            }

            .btns {
                position: absolute;
                top: 9%;
                left: 95%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                background-color: red;
                color: white;
                font-size: 16px;
                padding: 10px 14px;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-align: center;
            }

            .btns:hover {
                background-color: black;
            }
        </style>

        <div class="row">
            <div class="col-sm-1">
                <label for="Anons">Anons</label>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <textarea rows="10" cols="100" name="Anons" id="Anons"><?php echo $new['Anons'];?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <label for="f">Picture</label>
            </div>
            <div class="col-sm-4">
                <p><input type="file" onchange="Picture.call(this)" accept="image/*" id="f" name="f">
            </div>
        </div>
        <div class="row rows" id="PicturePlace" style="display:<?php echo $new['PictureFull']!=null ?  "block" :  "none";?>">
            <img id="imagePicture" src="<?php echo "img\\anons\\".$new['PictureFull'];?>" class="img-thumbnail image">
            <a id="btnP" onclick="changePicture()" class="btn btns">X</a>
        </div>
        <script>
            function changePicture() {
                var image = document.getElementById("imagePicture");
                image.src = "";
                document.getElementById("f").value="";
                var btn = document.getElementById("PicturePlace");
                btn.style.display="none";
            }
        </script>
        <script>
            function changeAnons() {
                var image = document.getElementById("imageAnons");
                image.src = "";
                document.getElementById("a").value="";
                var btn = document.getElementById("AnonsPlace");
                btn.style.display="none";

            }
        </script>
        <script>
            function Anon() {
                if(this.files && this.files[0])
                {
                    var obj = new FileReader();
                    obj.onload = function(data)
                    {
                        var image = document.getElementById("imageAnons");
                        image.src = data.target.result;
                        var btn = document.getElementById("AnonsPlace");
                        btn.style.display="block";
                    }
                    obj.readAsDataURL(this.files[0]);
                }
            }
        </script>
        <script>
            function Picture() {
                if(this.files && this.files[0])
                {
                    var obj = new FileReader();
                    obj.onload = function(data)
                    {
                        var image = document.getElementById("imagePicture");
                        image.src = data.target.result;
                        var btn = document.getElementById("PicturePlace");
                        btn.style.display = "block";
                    }
                    obj.readAsDataURL(this.files[0]);
                }
            }
        </script>
        <div class="row">
            <div class="col-sm-1">
                <input type="text" hidden="hidden" name="idUpd" id="idUpd" value="<?php echo $this->id;?>">
                <label for="nameNew">Name</label>
            </div>
            <div class="col-sm-4">
                <input type="text" name="nameUpd" id="nameUpd" value="<?php echo $new['name'];?>">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <label for="Text">Text</label>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <textarea rows="10" cols="100" name="textUpd" id="textUpd"><?php echo $new['text'];?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <label for="authorNew">Author</label>
            </div>
            <div class="col-sm-4">
                <input type="text" name="authorUpd" id="authorUpd" value="<?php echo $new['author'];?>">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <label for="dateNew">Date</label>
            </div>
            <div class="col-sm-4">
                <input type="date" name="dateUpd" id="dateUpd" value="<?php $date = new DateTime($new["date"]); echo $date->format("Y-m-d");?>">
            </div>
        </div>
        <button type="submit" name="UpdateBtn">Save</button>
            <button type="button" onClick='location.href="http://site.ru/admin"' >cancel</button>
        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Anons").editor({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#textUpd").editor({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>
</form>

</body>
</html>
<?php } ?>