
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="http://site.ru/">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <link rel="stylesheet" href="css/style.css">


</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-1">
            <input onClick='location.href="http://site.ru"' width="50" height="50" type="image" src="back.png" />
        </div>
        <div class="col-sm-4">
            <h4>Back to main page</h4>
        </div>
    </div>
</div>
<section class="subpage-bg">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="titile-block title-block_subpage">
                    <h2><?php echo $this->array['name']?></h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="gallery-block compact-gallery main-block">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-md-8 responsive-wrap">
                <div class="full-blog">
            <figure class="img-holder">
                <a class="lightbox" href="<?php echo "img/".$this->array['PictureFull']?>">
                    <img class="img-fluid" src="<?php echo "img/".$this->array['PictureFull']?>">
                </a>
                <div class="blog-post-date">
                    <?php $date=new DateTime($this->array['date']); echo $date->format("F j, Y")?>
                </div>
            </figure>
                    <div class="blog-content">
                        <p class="text-muted">By: <?php echo $this->array['author']?> </p>
                        <div class="blog-text">

                            <p class="lead">
                                <?php echo $this->array['text']?>
                            </p>
                        </div>
                    </div>
        </div>
            </div>
            <div class="col-md-4 responsive-wrap">
                <div class="sidebar">
                    <div class="widget-box">
                        <h4>Latest News</h4>
                        <?php foreach ($this->news AS $index): ?>
                        <div class="latest-blog">
                            <a href="/news/<?php echo $index['id']?>">
                                <img src="<?php echo "img/anons/".$index['PictureAnons']?>" alt="#">
                                <div class="blog-thumb-content">
                                    <p><strong><?php echo $index['name']?></strong></p>
                                    <p>By <?php echo $index['author']?> </p>
                                    <p> <?php $date=new DateTime($index['date']); echo $date->format("F j, Y")?> </p>
                                </div>
                            </a>

                        </div>
                        <?php endforeach; ?>

                    </div>
    </div>
            </div>
</section>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.compact-gallery',{animation:'slideIn'});
</script>

</body>
</html>