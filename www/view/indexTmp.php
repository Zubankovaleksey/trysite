<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <base href="http://site.ru/">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>The first site</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

</head>

<body>
  <!-- Start your project here-->
  <header>
      <nav id="menu" class="navbar scrolling-navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="index.php">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Nav" aria-controls="Nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="Nav">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                      <a class="nav-link" href="#intro">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#news">News</a>
                  </li>
                  <?php
                  if(!isset($this->name)){?>
                      <li class="nav-item">
                          <a href="#SignIn" class="nav-link" data-toggle="modal" data-target="#SingModal">SignIn</a>
                      </li>
                  <?php } ?>
                  <?php
                  if(!isset($this->name)){?>
                      <li class="nav-item">
                          <a href="#SignUp" class="nav-link" data-toggle="modal" data-target="#SingUpModal">SignUp</a>
                      </li>
                  <?php } ?>
                  <?php
                  if(isset($this->name)){?>
                      <li class="nav-item">
                          <a href="/logout" class="nav-link" name="SingOutLink">SignOut</a>
                      </li>
                  <?php } ?>
                  <?php
                  if($this->isAdmin){?>
                      <li class="nav-item">
                          <a href="/admin" class="nav-link">Admin</a>
                      </li>
                  <?php } ?>
              </ul>
              <ul class="navbar-nav">
                  <?php if($this->name){?>
                      <li class="nav-item">
                          <font color="white" size="5" face="serif">Hello, <?php print($this->name);?></font>
                      </li>
                  <?php }?>
              </ul>

          </div>
      </nav>

  	<div id="intro" class="view">
			<div class="mask rgba-black-strong">
				<div class="container-fluid d-flex align-items-center justify-content-center h-100">
					<div class="row d-flex justify-content-center text-center">
						<div class="col-md-10">
							<h2 class="display-4 font-weight-bold white-text pt-5 mb-2">
								first
							</h2>
							<hr class="hr-light">
							<h4 class="white-text my-4">first times</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
  </header>
  <section id="news" class="container-fluid text-center">
  	<h2 class="mb-5 font-weight-bold"> News</h2>
  	<div class="row">
        <?php foreach ($this->array AS $index): ?>
        <div class="col-md-4 col-sm-6">
            <a href="/news/<?php echo $index['id']?>">

            <div class="our-team">
                <div class="team-image">
                    <img src=<?php echo "img\\anons\\".$index["PictureAnons"]?>>
                    <p class="description">
                        <?php echo $index['Anons']?>
                    </p>

                </div>
                <div class="team-info">
                    <h3 class="title"><?php echo $index['name']?></h3>
                    <span class="post">author: <?php echo $index['author']?></span>
                </div>
            </div>
            </a>

        </div>
        <?php endforeach; ?>

  	</div>
  </section>
  <div class="modal fade" id="SingModal" tabindex="-1" role="dialog" aria-labelledby="SingModal" aria-hidden="true">
  	<div class="modal-dialog" role="document">
  		<div class="modal-content">
  			<div class="modal-header">
  				<h5 class="modal-title" id="ModalTitle">Authorisate</h5>
  				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
  				</button>
  			</div>
  			<div class="modal-body">
  				<div class="container-fluid">
  					<form class="loginForm" action="">
  						<div class="form-group">
  							<label for="login">login</label>
  							<input type="text" class="form-control" name="login" id="login" placeholder="login">
                            <div id="incorrectLogin" style="display:none;">Field is empty!</div>

                        </div>
  						<div class="form-group">
  							<label for="password">Password</label>
  							<input type="password" class="form-control" name="password" id="password" placeholder="Password" >
                            <div id="incorrectPassword" style="display:none;">Field is empty!</div>

  						</div>
  						<div id="incorrectData" style="display:none;">Incorrect login and/or password</div>
                        <input type="submit" class="btn btn-outline-secondary" name="LoginButton" value="Enter">
  					</form>
                    <script type="text/javascript">

                    </script>
  				</div>
  			</div>

  		</div>
  	</div>
  </div>
  <div class="modal fade" id="SingUpModal" tabindex="-1" role="dialog" aria-labelledby="SingModal" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		  <div class="modal-content">
			  <div class="modal-header">
				  <h5 class="modal-title" id="ModalTitleUp">Authorisate</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
				  </button>
			  </div>
			  <div class="modal-body">
				  <div class="container-fluid">
					  <form class="signUpForm" action="">
						  <div class="form-group">
							  <label for="loginSingUp">login</label>
							  <input type="text" class="form-control" name="loginSingUp" id="loginSingUp" placeholder="login">
                              <div id="incorrectSignUpLogin" style="display:none;">Incorrect login[4,30)</div>

                          </div>
						  <div class="form-group">
							  <label for="nameSingUp">name</label>
							  <input type="text" class="form-control" name="nameSingUp" id="nameSingUp" placeholder="name">
                              <div id="incorrectSignUpName" style="display:none;">Incorrect name[4,30)</div>

                          </div>
						  <div class="form-group">
							  <label for="passwordSingUp">Password</label>
							  <input type="password" class="form-control" name="passwordSingUp" id="passwordSingUp" placeholder="Password" >
                              <div id="incorrectSignUpPassword" style="display:none;">Incorrect password[4,30)</div>

						  </div>
						  <div id="exist" style="display:none;">user with this login already exist</div>
                          <input type="submit" class="btn btn-outline-secondary" name="singUpButton" value="SingUp"></input>
					  </form>
                      <script type="text/javascript" src="../js/login.js"></script>
                      <script type="text/javascript" src="../js/signUp.js"></script>


                  </div>
			  </div>

		  </div>
	  </div>
  </div>

  <!-- Start your project here-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>


</html>
